const express=require("express");
const router=express.Router();
//importamos el Schema
const usuarioSchema=require("../models/usuario.model");


// crear un nuevo  producto
router.post('/usuario',(req,res)=>{
    const usuario=usuarioSchema(req.body);
    usuario
    .save()
    .then((data)=>res.json(data))
    .catch((error)=>res.json({message: error}));
});

module.exports=router;