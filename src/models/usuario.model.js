const mongoose=require("mongoose");

const usuarioSchema=mongoose.Schema({
    
nickusuario: {
        type: String,
        required: true,
      },
      clave: {
        type: String,
      },
      tipousuario: {
        type: String,
        required: true,
      },
      nombres: {
        type: String,
        required: true,
      },
      apellidos: {
        type: String,
      },
    tipodoc: {
        type: String,
      },
      numdoc: {
        type: String,
      },
      direccion: {
        type: String,
      },
      telefono: {
        type: String,
      },
      email: {
        type: String,
      },
      estado: {
        type: String,
      }

});

module.exports=mongoose.model('usuarios',usuarioSchema);